# Game of Life:

## Goal of this project
 - Be able to create an GUI application from scratch and understand the 
   drawing **mechanics** (general mechanics, not just about java) for GUI 
   application
 - Designing a proper **user interface** that's user-friendly
 - Get familiar with **java**, **javafx**, and **gradle**

## Project requirements:
-  gradle version "6.6"
-  openjdk version "11.0.9.1"
-  javafx
-  Groovy

## Run the game

 This is a gradle project. Hence, as long as your current environment satisfy 
 the [project requirements](#project_requirements), you will be able to run 
 `gradle build` and `gradle run` to play the game.
 
 Note that this game proceeds 2 frames in one second, that is, it only moves forward 
 one frame every 0.5 seconds in the normal play mode.

## Rule of Conway's Game of Life

1. if a living cell has less than 2 neighbors or more than 3 neighbors, it dies
1. if a dead cell has 3 neighbors, it becomes alive

## Some main functionalities
 - The top toolbar provides a variety of different shapes that can be **drawn** on 
   the canvas by mouse clicking on the cells (e.g, press the button first 
   and click the cell on the canvas next).
    - Drawings made when a cell is clicked (by the mouse) are actually made to the next
    frame. **However**, a gray shape (only for visualization) will show up as a
    feedback to indicate that the mouse click is recorded, and the shape will be
    drawn in the next frame.


 - Pause and Resume the game with `Pause` and `Resume` buttons
   - Press [P] (on the keyboard) to pause or resume (play)
   - When the game is paused, the user can press [N] (on the keyboard)
     or press the button `Next` to move forward for one frame. Keep 
     pressing it for a while will change the frames rapidly.
     

 - A scroll bar for the main canvas will show up if the size of the canvas is 
   not enough to show all the cells.  


 - The bottom status bar keeps record of the following information (in order from 
   left to right):
   - Events happened to the main canvas.
   - The current population on the main canvas.
   - The current frame (since the beginning of the game).  
    

- Some hot-keys in general:  
  | Button          | Key   |
  | :-              | :-    |
  | Clear           | [ESC] |
  | Pause           | [P]   |
  | Play / Resume   | [P]   |
  | Next            | [N]   |

## Some details
 - The buttons have visually feedbacks (tighted straightly to the back end logic) when being pressed  
   - The graph buttons (i.e., Block, Beehive,...) can stay in the form of being "pressed" when clicked for the first time. They will "pop" back if the user clicked them again. "Obviously", the user will not be able to draw anything when no button is being "pressed".  
   - The functional buttons (i.e., Clear, Pause / Resume) will not stay in the "pressed" form since their changes are made immediately.  
   - When the user clicks any button, the user have to release the mouse inside the button area to make the desired operation work. That is, if the user released the mouse outside of the button area, the button will **not** respond.  
 - Each button has a tip that can be seen when the user moved the mouse onto the button for a while  



