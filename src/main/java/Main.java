import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main extends Application {
    /*************************  Main components of the screen ***************************/
    // the root
    BorderPane c_root = new BorderPane();
    // the main scene
    Scene c_scene;
    // three main components
    ToolBar c_toolBar;
    ScrollPane c_scrollPane;
    BorderPane c_statusBar;

    /************************* Predefined Sizes ***************************/
    final int c_row = 50;
    final int c_col = 75;
    final int c_length = 16;   // <= 21
    final int c_lengthOffSet = c_length/9;

    final int c_toolBarMaxHeight = 50;
    final int c_btnImgMaxHeight = 35;
    final int c_btnImgMaxWidth = 80;

    final int c_scrollBarWidth = 8;

    final int c_bottomBarMaxHeight = 30;
    final int c_CanvasHeight = c_length*c_row + c_scrollBarWidth;

    final int c_borderWidth = 1;

    final int c_WindowWidth = c_length*c_col + c_scrollBarWidth;

    final boolean c_DEBUG = false;

    /************************* Background and border style ***************************/
    // set background and border style
    Background c_bg = new Background(new BackgroundFill(Color.BEIGE,CornerRadii.EMPTY, Insets.EMPTY));
    Border c_bd = new Border(new BorderStroke(Color.GRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(c_borderWidth)));

    /************************* c_ticker and frames ***************************/
    // c_ticker to tick every 1 second
    Timer c_ticker;

    final int c_frameRate = 500;  // Each frame is displayed in every c_frameRate ms
    int c_frameCounter = 0;
    String c_shapeCreated = "";

    // population record
    int c_population = 0;

    // two boards to record the c_LIVE status of each cell (for current frame and the next frame)
    final int c_totalNBoard = 2;
    List<List<List<BOOL>>> c_gameBoards = new ArrayList<>();

    // c_actualBoard stores all the cells in order
    List<List<Rectangle>> c_actualBoard = new ArrayList<>();

    /************************* Button ***************************/
    // state recorded for different buttons
    BTNSTATE c_curState = BTNSTATE.NA;

    // current pressed button
    BTNSTATE c_curPressed = BTNSTATE.NA;

    enum BTNSTATE{
        NA,
        BLOCK,
        BEEHIVE,
        BLINKER,
        TOAD,
        GLIDER,
        CLEAR,
        PAUSE,
        PLAY,
        PROCEED
    }

    // Some macro definitions of ac_LIVE and c_DEAD
    final Color c_LIVE = Color.BLACK;
    final Color c_TEMP = Color.GRAY;
    final Color c_DEAD = Color.WHITE;

    // record all active buttons
    List<Button> c_btnList = new ArrayList<>();

    // Implementation for pause button
    // paused or not
    boolean c_isPaused = false;
    Button c_pauseButton;
    final ImageView c_pauseImgView = new ImageView(new Image("img/Pause.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,false));
    final ImageView c_playImgView = new ImageView(new Image("img/Play.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,true));

    Button c_clearButton;
    Button c_proceedButton;
    /************************* "Global" Widgets ***************************/
    Label c_eventLabel;
    Label c_frameLabel;
    Label c_populationLabel;

    Label c_toolBarLabel;

    /************************* Other ***************************/

    // indicate what is the shape chose
    SHAPEENUM c_shapeChose = SHAPEENUM.NA;

    enum SHAPEENUM{
        NA,
        BLOCK,
        BEEHIVE,
        BLINKER,
        TOAD,
        GLIDER,
        EATER,
        SNAKE,
        LOAF,
        ANTIGLIDER,
        LWSS,
        ANTILWSS,
        GOSPERGUN
    }

    Lock c_mutex = new ReentrantLock();

    // a subclass wraps a boolean value
    static class BOOL{
        private boolean value;
        public BOOL(boolean val){
            this.value = val;
        }
        boolean getVal(){ return this.value; }
        void turnOn(){ this.value = true; }
        void turnOff(){ this.value = false; }
    }

    /********************************** Initializing Functions ************************************/
    /**
     * initialize everything for the window
     */
    private void initialization(Stage stage){

        stage.setTitle("Conway's Game of Life");
        c_scene = new Scene(c_root,c_WindowWidth, c_CanvasHeight+c_toolBarMaxHeight + c_bottomBarMaxHeight);

        // initialize three main components
        initializeToolBar();
        initializeBoard();
        initializeStatusBar();

        // add components to the root
        c_root.setTop(c_toolBar);
        c_root.setCenter(c_scrollPane);
        c_root.setBottom(c_statusBar);

        // initialize key events
        initializeKeyEvents();

        // initialize the ticker (Reference: code from piazza@265)
        c_ticker = new Timer();
        c_ticker.scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run() {
                updateBoard();
            }
        }, 0, c_frameRate);

        // stop the c_ticker when the window is closed
        stage.setOnCloseRequest(event -> c_ticker.cancel());
    }

    private void initializeKeyEvents(){

        // set key combination for Clear
        KeyCombination kcProceed = new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_ANY);
        c_scene.getAccelerators().put(kcProceed, new Runnable() {
            @Override
            public void run() {
                c_proceedButton.fire();
            }
        });

        // set key combination for pause and play
        KeyCombination kcPause = new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_ANY);
        c_scene.getAccelerators().put(kcPause, new Runnable() {
            @Override
            public void run() {
                c_pauseButton.fire();
            }
        });

        // set key combination for Clear
        KeyCombination kcClear = new KeyCodeCombination(KeyCode.ESCAPE, KeyCombination.CONTROL_ANY);
        c_scene.getAccelerators().put(kcClear, new Runnable() {
            @Override
            public void run() {
                c_clearButton.fire();
            }
        });
    }

    /**
     * initialize the toolbar
     */
    private void initializeToolBar(){

        /******************** Buttons on the toolbar ********************/
        // Block Button
        // initialize the buttons
        Image blockImg = new Image("img/Block.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,false);
        Button blockButton= new Button("Block",new ImageView(blockImg));
        blockButton.setMaxHeight(c_toolBarMaxHeight);

        // set tool tip
        Tooltip blockTip = new Tooltip("Draw a block.");
        blockButton.setTooltip(blockTip);

        // add component to the parent
        c_btnList.add(blockButton);

        // set button events
        setButtonEvents(blockButton,BTNSTATE.BLOCK,SHAPEENUM.BLOCK);

        // Beehive Button
        Image beehiveImg = new Image("img/Beehive.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,false);
        Button beehiveButton= new Button("Beehive",new ImageView(beehiveImg));
        beehiveButton.setMaxHeight(c_toolBarMaxHeight);
        Tooltip beehiveTip = new Tooltip("Draw a beehive.");
        beehiveButton.setTooltip(beehiveTip);
        c_btnList.add(beehiveButton);
        setButtonEvents(beehiveButton,BTNSTATE.BEEHIVE,SHAPEENUM.BEEHIVE);

        // Blinker Button
        Image blinkerImg = new Image("img/Blinker.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,false);
        Button blinkerButton= new Button("Blinker",new ImageView(blinkerImg));
        blinkerButton.setMaxHeight(c_toolBarMaxHeight);
        Tooltip blinkerTip = new Tooltip("Draw a blinker.");
        blinkerButton.setTooltip(blinkerTip);
        c_btnList.add(blinkerButton);
        setButtonEvents(blinkerButton,BTNSTATE.BLINKER,SHAPEENUM.BLINKER);

        // Toad Button
        Image toadImg = new Image("img/Toad.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,false);
        Button toadButton= new Button("Toad",new ImageView(toadImg));
        toadButton.setMaxHeight(c_toolBarMaxHeight);
        Tooltip toadTip = new Tooltip("Draw a toad.");
        toadButton.setTooltip(toadTip);
        c_btnList.add(toadButton);
        setButtonEvents(toadButton,BTNSTATE.TOAD,SHAPEENUM.TOAD);

        // Block Button
        Image gliderImg = new Image("img/Glider.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,false);
        Button gliderButton= new Button("Glider",new ImageView(gliderImg));
        gliderButton.setMaxHeight(c_toolBarMaxHeight);
        Tooltip gliderTip = new Tooltip("Draw a glider.");
        gliderButton.setTooltip(gliderTip);
        c_btnList.add(gliderButton);
        setButtonEvents(gliderButton,BTNSTATE.GLIDER,SHAPEENUM.GLIDER);

        // Clear Button
        Image clearImg = new Image("img/Clear.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,true);
        c_clearButton= new Button("Clear [ESC]",new ImageView(clearImg));
        c_clearButton.setMaxHeight(c_toolBarMaxHeight);
        Tooltip clearTip = new Tooltip("Press [ESC] to clear the canvas (a.k.a. wipe all lives).");
        c_clearButton.setTooltip(clearTip);
        c_btnList.add(c_clearButton);
        setClearButtonEvents();

        // Pause / Play Button
        c_pauseButton= new Button("Pause [P]", c_pauseImgView);
        c_pauseButton.setMaxHeight(c_toolBarMaxHeight);
        c_btnList.add(c_pauseButton);
        setPauseButtonEvents();

        // Proceed Button
        Image proceedImg = new Image("img/Proceed.png",c_btnImgMaxWidth,c_btnImgMaxHeight,true,true);
        c_proceedButton = new Button("Next [N]",new ImageView(proceedImg));
        c_proceedButton.setMaxHeight(c_toolBarMaxHeight);
        setProceedButtonEvents();
        setProceedButtonVisible(false);
        c_btnList.add(c_proceedButton);

        /******************** Menus on the toolbar ********************/
        // initialize the menuBar for still shapes
        MenuBar menubarStillShapes = new MenuBar();
        Menu otherStillShapes = new Menu("More");
        MenuItem eaterItem = new MenuItem("Eater");
        MenuItem snakeItem = new MenuItem("Snake");
        MenuItem loafItem = new MenuItem("Loaf");

        // add functionalities to the menu items
        eaterItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.EATER;resetAllBtnEffects();c_curState = BTNSTATE.NA;});
        snakeItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.SNAKE;resetAllBtnEffects();c_curState = BTNSTATE.NA;});
        loafItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.LOAF;resetAllBtnEffects();c_curState = BTNSTATE.NA;});

        // add menu items into the main menu root
        otherStillShapes.getItems().addAll(eaterItem,snakeItem,loafItem);

        menubarStillShapes.getMenus().add(otherStillShapes);
        menubarStillShapes.setPrefHeight(c_toolBarMaxHeight);
        menubarStillShapes.setPadding(new Insets((float)c_toolBarMaxHeight/4,0,3,0));

        // initialize the menuBar for active shapes
        MenuBar menubarActiveShapes = new MenuBar();
        Menu otherActiveShapes = new Menu("More");
        MenuItem antiGliderItem = new MenuItem("Antiglider");
        MenuItem LWSSItem = new MenuItem("LWSS");
        MenuItem antiLWSSItem = new MenuItem("AntiLWSS");
        MenuItem gosperGunItem = new MenuItem("Gosper's Gun");

        // add functionalities to the menu items
        antiGliderItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.ANTIGLIDER;resetAllBtnEffects();c_curState = BTNSTATE.NA;});
        LWSSItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.LWSS;resetAllBtnEffects();c_curState = BTNSTATE.NA;});
        antiLWSSItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.ANTILWSS;resetAllBtnEffects();c_curState = BTNSTATE.NA;});
        gosperGunItem.setOnAction(actionEvent -> {c_shapeChose = SHAPEENUM.GOSPERGUN;resetAllBtnEffects();c_curState = BTNSTATE.NA;});

        // add menu items into the main menu root
        otherActiveShapes.getItems().addAll(antiGliderItem,LWSSItem,antiLWSSItem,gosperGunItem);

        menubarActiveShapes.getMenus().add(otherActiveShapes);
        menubarActiveShapes.setPrefHeight(c_toolBarMaxHeight);
        menubarActiveShapes.setPadding(new Insets((float)c_toolBarMaxHeight/4,0,3,0));

        /******************** Organize the toolbar ********************/
        // add components to the root (ToolBar)
        Separator sepV1 = new Separator();
        Separator sepV2 = new Separator();
        Separator sepV3 = new Separator();
        Separator sepV4 = new Separator();

        c_toolBarLabel = new Label("");
        c_toolBarLabel.setWrapText(true);
        c_toolBarLabel.setMaxWidth(250);

        c_toolBar = new ToolBar(blockButton, beehiveButton,menubarStillShapes,
                sepV1,sepV2,
                blinkerButton, toadButton, gliderButton,menubarActiveShapes,
                sepV3,sepV4,
                c_clearButton, c_pauseButton, c_proceedButton, c_toolBarLabel);

        c_toolBar.setBackground(c_bg);
        c_toolBar.setBorder(c_bd);
        c_toolBar.setMinHeight(c_toolBarMaxHeight);
    }

    /**
     * initialize cells inside the game board
     */
    private void initializeBoard(){
        // initialize the two game boards
        List<List<BOOL>> gameBoard1 = new ArrayList<>();
        List<List<BOOL>> gameBoard2 = new ArrayList<>();

        Pane canvas = new Pane();

        for(int i = 0;i<c_col;++i){
            List<Rectangle> tmp = new ArrayList<>();

            // initialize the two game boards
            List<BOOL> gameBoardTmp1 = new ArrayList<>();
            List<BOOL> gameBoardTmp2 = new ArrayList<>();
            for(int j = 0;j<c_row;++j) {

                // the outside rectangle
                Rectangle outsideFrame = new Rectangle();
                outsideFrame.setX(i*c_length);
                outsideFrame.setY(j*c_length);
                outsideFrame.setWidth(c_length);
                outsideFrame.setHeight(c_length);
                outsideFrame.setFill(Color.WHITE);
                outsideFrame.setStroke(Color.BLACK);

                // the actual cell
                Rectangle innerSquare = new Rectangle();
                innerSquare.setX(i*c_length+c_lengthOffSet);
                innerSquare.setY(j*c_length+c_lengthOffSet);
                innerSquare.setWidth(c_length-2*c_lengthOffSet);
                innerSquare.setHeight(c_length-2*c_lengthOffSet);
                innerSquare.setFill(this.c_DEAD);

                setCellAction(innerSquare);
                tmp.add(innerSquare);
                canvas.getChildren().add(outsideFrame);
                canvas.getChildren().add(innerSquare);

                // initialize the two game boards
                gameBoardTmp1.add(new BOOL(false));
                gameBoardTmp2.add(new BOOL(false));

            }
            c_actualBoard.add(tmp);

            // initialize the two game boards
            gameBoard1.add(gameBoardTmp1);
            gameBoard2.add(gameBoardTmp2);
        }

        c_gameBoards.add(gameBoard1);
        c_gameBoards.add(gameBoard2);

        // add components to the scroll Pane
        c_scrollPane = new ScrollPane();
        c_scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);
        c_scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.AS_NEEDED);

        c_scrollPane.setContent(canvas);

        // modify scroll speed
        c_scrollPane.getContent().setOnScroll(scrollEvent -> {
            c_scrollPane.setVvalue(c_scrollPane.getVvalue() - scrollEvent.getDeltaY() * 0.005);
            c_scrollPane.setHvalue(c_scrollPane.getHvalue() - scrollEvent.getDeltaX() * 0.005);
        });
    }

    /**
     * initialize the status bar
     */
    private void initializeStatusBar(){
        // initialize labels
        c_eventLabel = new Label("Cleared.");
        c_frameLabel = new Label("Frame "+c_frameCounter);
        c_frameLabel.setPadding(new Insets(0,10,0,0));

        c_populationLabel = new Label("Population: "+c_population);

        // add components to the status bar
        c_statusBar = new BorderPane();
        c_statusBar.setLeft(c_eventLabel);
        c_statusBar.setCenter(c_populationLabel);
        c_statusBar.setRight(c_frameLabel);

        c_statusBar.setBackground(c_bg);
        c_statusBar.setBorder(c_bd);
        c_statusBar.setMaxHeight(c_bottomBarMaxHeight);
    }


    /********************************** Components Event settings ************************************/
    private void resetAllBtnEffects(){
        // reset all effect
        for(Button eachButton : c_btnList){
            eachButton.setEffect(null);
        }
    }

    /**
     * Style used for normal button
     * @param b the input button
     * @param state the state indicating the button
     */
    private void setButtonEvents(Button b, BTNSTATE state, SHAPEENUM shape){

        setMouseHoverEffect(b,state);

        // set state if the button is pressed / clicked
        b.setOnMousePressed(MouseEvent-> {
            c_curPressed = state;
            b.setEffect(new InnerShadow());
        });

        // button is fired
        b.setOnAction(ActionEvent -> {
            // button was not pressed, press it
            if(c_curState != state){
                c_curState = state;
                c_shapeChose = shape;
                // reset all effect
                resetAllBtnEffects();
                // add effect
                b.setEffect(new InnerShadow());
            }else{
                // button was already pressed, pop it

                // change state to NA
                c_curState = BTNSTATE.NA;
                c_shapeChose = SHAPEENUM.NA;
                // reset all effect
                resetAllBtnEffects();
            }
        });

        // reset effect if the button is not toggled
        b.setOnMouseReleased(MouseEvent ->{
            c_curPressed = BTNSTATE.NA;
            if(c_curState != state){
                b.setEffect(null);
            }else{
                b.setEffect(new InnerShadow());
            }
        });
    }

    /**
     * set the button events for Clear button
     */
    private void setClearButtonEvents(){
        // add shadow when mouse entered
        c_clearButton.setOnMouseEntered(MouseEvent -> {
            if(BTNSTATE.CLEAR != c_curPressed){
                c_clearButton.setEffect(new DropShadow());
            }
        });

        // remove shadow when mouse exited
        c_clearButton.setOnMouseExited(MouseEvent ->{
            if(BTNSTATE.CLEAR != c_curPressed){
                c_clearButton.setEffect(null);
            }
        });

        // if the button is actually pressed, clear the grid
        c_clearButton.setOnAction(actionEvent -> clearGrid());

        // add effect if mouse pressed
        c_clearButton.setOnMousePressed(MouseEvent->{
            c_curPressed = BTNSTATE.CLEAR;
            // add effect
            c_clearButton.setEffect(new InnerShadow());
        });

        // remove the effect when button is released
        c_clearButton.setOnMouseReleased(MouseEvent->{
            c_curPressed = BTNSTATE.NA;
            c_clearButton.setEffect(null);
        });
    }

    /**
     * set the button events for pause button
     */
    private void setPauseButtonEvents(){
        // set fixed width
        c_pauseButton.setPrefWidth(110);

        // set mouse hover effects
        c_pauseButton.setOnMouseEntered(MouseEvent ->{
            if((c_isPaused && BTNSTATE.PLAY != c_curPressed) || (!c_isPaused && BTNSTATE.PAUSE != c_curPressed)){
                c_pauseButton.setEffect(new DropShadow());
            }
        });

        // remove shadow when mouse exited
        c_pauseButton.setOnMouseExited(MouseEvent ->{
            if((c_isPaused && BTNSTATE.PLAY != c_curPressed) || (!c_isPaused && BTNSTATE.PAUSE != c_curPressed)){
                c_pauseButton.setEffect(null);
            }
        });

        // Pause / Play button
        Tooltip isPausedTip = new Tooltip("Press [P] to Resume the game.");
        Tooltip notPausedTip = new Tooltip("Press [P] to Pause the game.");
        c_pauseButton.setTooltip(notPausedTip);

        // set action for pause button
        c_pauseButton.setOnAction(actionEvent -> {
            if(c_isPaused){
                // is paused, so resume
                c_isPaused = false;

                c_pauseButton.setTooltip(notPausedTip);
                setProceedButtonVisible(false);

                // resume ticker
                c_ticker = new Timer();
                c_ticker.scheduleAtFixedRate(new TimerTask(){
                    @Override
                    public void run() {
                        updateBoard();
                    }
                }, 0, c_frameRate);

                c_pauseButton.setGraphic(c_pauseImgView);
                c_pauseButton.setText("Pause [P]");

            }else{
                // not paused, so pause
                c_isPaused = true;

                c_pauseButton.setTooltip(isPausedTip);
                setProceedButtonVisible(true);

                // stop ticker
                c_ticker.cancel();

                c_pauseButton.setGraphic(c_playImgView);
                c_pauseButton.setText("Play [P]");
            }
        });

        // set effect when pressed
        c_pauseButton.setOnMousePressed(MouseEvent->{
            if(c_isPaused){
                c_curPressed=BTNSTATE.PLAY;
            }else{
                c_curPressed=BTNSTATE.PAUSE;
            }
            // add effect
            c_pauseButton.setEffect(new InnerShadow());
        });

        // reset the effect if mouse is released
        c_pauseButton.setOnMouseReleased(MouseEvent->{
            c_curPressed = BTNSTATE.NA;
            c_pauseButton.setEffect(null);
        });
    }

    /**
     * set the button events for pause button
     */
    private void setProceedButtonEvents(){
        // set tool tip
        Tooltip isPausedTip = new Tooltip("Press [N] to move forward a frame. Keep it pressed will move forward rapidly.");
        c_proceedButton.setTooltip(isPausedTip);

        // set mouse hover effects
        c_proceedButton.setOnMouseEntered(MouseEvent ->{
            if(BTNSTATE.PROCEED != c_curPressed){
                c_proceedButton.setEffect(new DropShadow());
            }
        });

        // remove shadow when mouse exited
        c_proceedButton.setOnMouseExited(MouseEvent ->{
            if(BTNSTATE.PROCEED != c_curPressed){
                c_proceedButton.setEffect(null);
            }
        });

        // set action for pause button
        c_proceedButton.setOnAction(actionEvent -> {
            if(c_isPaused){
                updateBoard();
            }
        });

        // set effect when pressed
        c_proceedButton.setOnMousePressed(MouseEvent->{
            c_curPressed=BTNSTATE.PROCEED;
            // add effect
            c_proceedButton.setEffect(new InnerShadow());
        });

        // reset the effect if mouse is released
        c_proceedButton.setOnMouseReleased(MouseEvent->{
            c_curPressed = BTNSTATE.NA;
            c_proceedButton.setEffect(null);
        });
    }

    /**
     *
     * @param b Input Button
     * @param state the state corresponding to the button
     */
    private void setMouseHoverEffect(Button b, BTNSTATE state){
        // add shadow when mouse entered
        b.setOnMouseEntered(MouseEvent -> { if(c_curPressed != state){ b.setEffect(new DropShadow()); }});

        // remove shadow when mouse exited
        b.setOnMouseExited(MouseEvent ->{
            if(state != c_curPressed){
                if(state != c_curState){
                    b.setEffect(null);
                }else{
                    b.setEffect(new InnerShadow());
                }
            }
        });
    }

    /**
     *
     * @param show show or not
     */
    private void setProceedButtonVisible(boolean show){
        if(show){
            c_proceedButton.setVisible(true);
        }else{
            c_proceedButton.setVisible(false);
        }
    }

    /**
     * set the passive actions for each cell
     */
    private void setCellAction(Rectangle r){
        r.setOnMouseClicked(mouseEvent -> {

            int x = (int)(r.getX()-c_lengthOffSet) / c_length;
            int y = (int)(r.getY()-c_lengthOffSet) / c_length;

            if(c_DEBUG){
                System.out.println("[DEBUG] Cell clicked: x:"+x+", y:" + y);
            }

            drawShape(x,y);
        });
    }

    /********************************** Canvas ("Rendering") Functions ************************************/
    /**
     * draw the shape on the game board depending on c_shapeChose
     */
    private void drawShape(int x, int y){

        c_mutex.lock();

        // draw the shapes on the next board
        List<List<BOOL>> gameBoard = getNxtBoard();

        c_shapeCreated = null;

        if(c_DEBUG) {
            System.out.println("[DEBUG] draw shape at (" + x + "," + y + ")");
        }

        switch(c_shapeChose){
            case BLOCK:
                c_shapeCreated = "block";

                // line 1
                try{setCoordLive(gameBoard,x,y); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y); c_population++; }catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y+1); c_population++; }catch (Exception ignore){}

                break;
            case BEEHIVE:
                c_shapeCreated = "beehive";

                // line 1
                try{setCoordLive(gameBoard,x+1,y); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y); c_population++; }catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y+1); c_population++; }catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x+1,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+2); c_population++; }catch (Exception ignore){}

                break;
            case BLINKER:
                c_shapeCreated = "blinker";

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+1); c_population++; }catch (Exception ignore){}

                break;
            case TOAD:
                c_shapeCreated = "toad";

                // line 1
                try{setCoordLive(gameBoard,x+1,y); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y); c_population++; }catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+1); c_population++; }catch (Exception ignore){}

                break;
            case ANTIGLIDER:
                c_shapeCreated = "antiglider";
                // line 1
                try{setCoordLive(gameBoard,x,y); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y); c_population++; }catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+1); c_population++; }catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x,y+2); c_population++;}catch (Exception ignore){}
                break;

            case GLIDER:
                c_shapeCreated = "glider";

                // line 1
                try{setCoordLive(gameBoard,x+2,y); c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+1); c_population++; }catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x+1,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+2); c_population++; }catch (Exception ignore){}

                break;
            case EATER:
                c_shapeCreated = "eater";
                // line 1
                try{setCoordLive(gameBoard,x,y);c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y);c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+1); c_population++; }catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x+2,y+2); c_population++; }catch (Exception ignore){}

                // line 4
                try{setCoordLive(gameBoard,x+2,y+3); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y+3); c_population++;}catch (Exception ignore){}
                break;

            case SNAKE:
                c_shapeCreated = "snake";
                // line1
                try{setCoordLive(gameBoard,x,y); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y); c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y+1); c_population++;}catch (Exception ignore){}
                break;

            case LOAF:
                c_shapeCreated = "loaf";
                // line1
                try{setCoordLive(gameBoard,x+1,y); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y); c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y+1); c_population++;}catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x+1,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y+2); c_population++;}catch (Exception ignore){}

                // line 4
                try{setCoordLive(gameBoard,x+2,y+3); c_population++; }catch (Exception ignore){}
                break;

            case LWSS:
                c_shapeCreated = "LWSS";
                // line1
                try{setCoordLive(gameBoard,x,y); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y); c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+4,y+1); c_population++;}catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x,y+2); c_population++; }catch (Exception ignore){}

                // line 4
                try{setCoordLive(gameBoard,x+1,y+3); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+4,y+3); c_population++;}catch (Exception ignore){}
                break;

            case ANTILWSS:
                c_shapeCreated = "AntiLWSS";
                // line 1
                try{setCoordLive(gameBoard,x,y); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+4,y); c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x+4,y+1); c_population++; }catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+4,y+2); c_population++;}catch (Exception ignore){}

                // line4
                try{setCoordLive(gameBoard,x+1,y+3); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+2,y+3); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+3,y+3); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+4,y+3); c_population++;}catch (Exception ignore){}
                break;

            case GOSPERGUN:
                c_shapeCreated = "Gosper's Gun";
                // line 1
                try{setCoordLive(gameBoard,x+24,y); c_population++;}catch (Exception ignore){}

                // line 2
                try{setCoordLive(gameBoard,x+22,y+1); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+24,y+1); c_population++; }catch (Exception ignore){}

                // line 3
                try{setCoordLive(gameBoard,x+12,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+13,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+20,y+2); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+21,y+2); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+34,y+2); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+35,y+2); c_population++;}catch (Exception ignore){}

                // line 4
                try{setCoordLive(gameBoard,x+11,y+3); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+15,y+3); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+20,y+3); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+21,y+3); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+34,y+3); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+35,y+3); c_population++;}catch (Exception ignore){}

                // line 5
                try{setCoordLive(gameBoard,x,y+4); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y+4); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+10,y+4); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+16,y+4); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+20,y+4); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+21,y+4); c_population++;}catch (Exception ignore){}

                // line 6
                try{setCoordLive(gameBoard,x,y+5); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+1,y+5); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+10,y+5); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+14,y+5); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+16,y+5); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+17,y+5); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+22,y+5); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+24,y+5); c_population++;}catch (Exception ignore){}

                // line 7
                try{setCoordLive(gameBoard,x+10,y+6); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+16,y+6); c_population++;}catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+24,y+6); c_population++;}catch (Exception ignore){}

                // line 8
                try{setCoordLive(gameBoard,x+11,y+7); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+15,y+7); c_population++;}catch (Exception ignore){}

                // line 9
                try{setCoordLive(gameBoard,x+12,y+8); c_population++; }catch (Exception ignore){}
                try{setCoordLive(gameBoard,x+13,y+8); c_population++;}catch (Exception ignore){}
                break;

            default:
                c_shapeCreated = null;
        }

        // update the status bar
        if(c_shapeCreated != null){
            Platform.runLater(() -> c_eventLabel.setText("Created "+c_shapeCreated+" at ("+ x + "," + y + ")"));
        }

        c_mutex.unlock();
    }

    /**
     * Clear the game board (set all cells to white)
     */
    private void clearGrid(){
        c_mutex.lock();

        // reset population
        c_population = 0;

        for(int i=0;i<c_col;++i){
            for(int j=0;j<c_row;++j){
                c_gameBoards.get(0).get(i).get(j).turnOff();
                c_gameBoards.get(1).get(i).get(j).turnOff();
                c_actualBoard.get(i).get(j).setFill(this.c_DEAD);
            }
        }

        c_eventLabel.setText("Cleared.");
        c_mutex.unlock();
    }

    /**
     * update function for the board (called every frame)
     */
    private void updateBoard(){
        c_mutex.lock();

        //******************* draw current frame (draw the window) **********************//
        // update frame counter
        c_frameCounter++;

        // update population
        Platform.runLater(() ->c_populationLabel.setText("Population: "+c_population));

        // update frameLabel (Referenced from piazza@265)
        Platform.runLater(() -> c_frameLabel.setText("Frame "+c_frameCounter));

        // draw current board onto the canvas (which was the next board)
        drawBoard();

        //************** update next frame (prepare for the next window) ******************//
        // update (render) the next board
        boolean alive;
        int counter;

        // Updates are based on current game board
        List<List<BOOL>> gameBoard1 = getCurBoard();
        List<List<BOOL>> gameBoard2 = getNxtBoard();

        // reset population and start incrementing
        int popTmp = 0;

        for(int i=0;i<c_col;++i){
            for(int j=0;j<c_row;++j){
                // initialize basic information about the cell
                alive = gameBoard1.get(i).get(j).getVal();
                counter = countLiveNeighbor(gameBoard1,i,j);

                if(counter == 3 || (alive && counter == 2)){
                    // turn on the cell if it's alive and exists exactly 2 or 3 live neighbors
                    // or if the cell is DEAD but has exactly 3 live neighbors
                    gameBoard2.get(i).get(j).turnOn();
                    popTmp++;
                }else{
                    // otherwise, mark it as DEAD
                    gameBoard2.get(i).get(j).turnOff();
                }
            }
        }
        c_mutex.unlock();

        c_population = popTmp;
    }

    /********************************** Helper Functions ************************************/
    /**
     *
     * @param x coordinate
     * @param y coordinate
     * @return the number of live neighbors based on current game board
     */
    private int countLiveNeighbor(List<List<BOOL>> gb, int x,int y){
        int counter = 0;

        // line above
        try{if(gb.get(x-1).get(y-1).getVal()){counter++;}}catch(Exception ignore){}
        try{if(gb.get(x).get(y-1).getVal()){counter++;}}catch(Exception ignore){}
        try{if(gb.get(x+1).get(y-1).getVal()){counter++;}}catch(Exception ignore){}

        // line in the middle
        try{if(gb.get(x-1).get(y).getVal()){counter++;}}catch(Exception ignore){}
        try{if(gb.get(x+1).get(y).getVal()){counter++;}}catch(Exception ignore){}

        // line below
        try{if(gb.get(x-1).get(y+1).getVal()){counter++;}}catch(Exception ignore){}
        try{if(gb.get(x).get(y+1).getVal()){counter++;}}catch(Exception ignore){}
        try{if(gb.get(x+1).get(y+1).getVal()){counter++;}}catch(Exception ignore){}

        return counter;
    }

    /**
     * draw current board onto the canvas
     */
    private void drawBoard(){
        c_mutex.lock();

        List<List<BOOL>> gameBoard = getCurBoard();
        for(int i=0;i<c_col;++i){
            for(int j=0;j<c_row;++j) {
                if(gameBoard.get(i).get(j).getVal()){
                    // set to Live
                    c_actualBoard.get(i).get(j).setFill(this.c_LIVE);
                }else{
                    // set to Dead
                    c_actualBoard.get(i).get(j).setFill(this.c_DEAD);
                }
            }
        }

        c_mutex.unlock();
    }

    /**
     *
     * @return current game board
     */
    private List<List<BOOL>> getCurBoard(){
        return c_gameBoards.get(c_frameCounter%c_totalNBoard);
    }

    /**
     *
     * @return next game board
     */
    private List<List<BOOL>> getNxtBoard(){
        return c_gameBoards.get((c_frameCounter+1)%c_totalNBoard);
    }

    /**
     * set board cell to alive
     * @param gameBoard the gameBoard we want to set
     * @param x coordinate
     * @param y coordinate
     */
    private void setCoordLive(List<List<BOOL>> gameBoard, int x,int y){
        gameBoard.get(x).get(y).turnOn();
        c_actualBoard.get(x).get(y).setFill(this.c_TEMP);
    }

    /********************************** Entry Function ************************************/
    /**
     * Entry function
     * @param stage the stage for the application
     */
    @Override
    public void start(Stage stage) {

        // initialize game board
        initialization(stage);

        // show starting scene
        stage.setScene(c_scene);
        stage.show();
    }
}
